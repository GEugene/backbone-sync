var MyApplication = MyApplication || {};
MyApplication.models = MyApplication.models || {};

_.extend(MyApplication, {
	registeredModels: [],
	registerModel: function(model) {
		if (_.indexOf(this.registeredModels, model) === -1) {
			MyApplication.registeredModels.push(model);
		}
	},
	unregisterModel: function(model) {
		this.registeredModels = _.without(this.registeredModels, model);
	}
});

/**
 * Функция переопределена для поддержки архитектуры приложения:
 * в ответ на любой запрос из модели приходит ответ со всей структурой приложения
 * (всеми данными).
 *
 * Каждая модель, которая хочет получать такие обновления,
 * должна наследоваться от MyApplication.models.Model.
 *
 * Следует иметь в виду, что только модель-инициатор после приема данных
 * будет генерировать, как обычно, событие "sync" (для нее вызывается оригинальный callback).
 * Для наполнения остальных моделей полученными данными будет использоваться метод set
 * без дополнительных опций.
 *
 * @param method
 * @param model
 * @param options
 * @returns {*}
 */
MyApplication.sync = function(method, model, options) {
	options || (options = {});

	var originalSuccess = options.success;
	options.success = function(resp) {
		/**
		 * Вызываем success callback для модели - инициатора запроса.
		 */
		originalSuccess && originalSuccess(resp);

		/**
		 * Вручную обновляем остальные модели.
		 */
		var manualUpdateModels = _.filter(MyApplication.registeredModels,
			function(regModel) { return regModel !== model; });

		_.each(manualUpdateModels, function(regModel) {
			/**
			 * TODO: проверить, правильно ли работает частичный set
			 * Можно отличать модель от коллекции по входящим данным (_.isArray, _.isObject)
			 */
			var data = regModel.parse(resp);
			regModel.set(data);

			/**
			 * NOTE: сортировать коллекцию предлагается в клиентском коде.
			 */
			//regModel.set(data, {sort: true});

			/**
			 * NOTE: этот вариант не подходит. Нам надо отслеживать "точечные" изменения.
			 */
			//regModel.set(data, {silent: true});
			//regModel.trigger('sync', regModel, resp, options);
		});
	};

	return Backbone.sync.apply(this, arguments);
};

MyApplication.models.Model = Backbone.Model.extend({
	initialize: function() {
		MyApplication.registerModel(this);
	},
	sync: function() {
		return MyApplication.sync.apply(this, arguments);
	},
	getId: function() {
		return this.get(this.idAttribute);
	},
	setId: function(id) {
		this.set(this.idAttribute, id);
	}
});