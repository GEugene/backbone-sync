Переопределяет **Backbone.sync** для поддержки архитектуры, когда в ответ на любой запрос из любой модели приходит ответ со всеми данными. Т.е. практически перезагрузка страницы без перезагрузки :)

Каждая модель, которая хочет получать такие обновления, должна наследоваться от MyApplication.models.Model.

Следует иметь в виду, что только модель-инициатор после приема данных будет генерировать, как обычно, событие "sync" (для нее вызывается оригинальный callback). Для наполнения остальных моделей полученными данными будет использоваться метод set без дополнительных опций.